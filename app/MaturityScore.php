<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Cache;

class MaturityScore extends Model
{
	protected $files;

	public $commitCategories = [
        'fix'       		=> [],
		'feature'   		=> [],
		'uncategorized'		=> [],
		'empty'				=> [],
    ];

	public function getCounts(){
		$counts = [        
		'fix'       		=> [],
		'feature'   		=> [],
		'uncategorized'		=> [],
		'empty'				=> []
		];

		foreach ($this->commitCategories as $key => $cats) {
			$counts[$key]['commits'] 	= count($cats);
			// $counts[$key]['comments'] 	= $this->countComments();
		}

		return $counts;
	}


	public function	externalDocumentation($readme){
		return preg_match('/example|demo|live/i', $readme);
	}

	public function	apiSection($readme){
		return preg_match('/api|options/i', $readme);
	}

	public function	gettingStarted($readme){
		return preg_match('/Getting Started|Installation|Usage/i', $readme);
	}

	public function getFiles($git){
		if (!empty($this->files))
			return $this->files;

		return $this->files = $git->getTrees();		
	}

	public function containsTestCases($git){
		foreach	($this->getFiles($git) as $file){
			if (stripos($file->path, 'phpunit.xml') !== false)	return true;
			if (stripos($file->path, 'test/') !== false)	return true;
		}

		return false;

	}
	

	public function containsBuildScripts($git){
		foreach	($this->getFiles($git) as $file){
			if (stripos($file->path, 'gulp') !== false)	return true;
			if (stripos($file->path, 'grunt') !== false)	return true;
			if (stripos($file->path, 'build/') !== false)	return true;
			if (stripos($file->path, 'dist/') !== false)	return true;
			if (stripos($file->path, 'build.xml') !== false)	return true;
			if (stripos($file->path, 'MAKEFILE') !== false)	return true;
			
		}

		return false;

	}

	public function containsCI($git){
		foreach	($this->getFiles($git) as $file){
			if (stripos($file->path, 'circle.yml') !== false)	return true;
			if (stripos($file->path, 'travis.yml') !== false)	return true;
			if (stripos($file->path, 'bamboo.yml') !== false)	return true;
		}

		return false;

	}


	public function containsPackageJSON($git){
		foreach	($this->getFiles($git) as $file){
			if (stripos($file->path, 'package.json') !== false)
				return true;
		}

		return false;
	}
	
	/**
	 * Undocumented function
	 *
	 * @param \App\GitHub $git
	 * @return void
	 */
	public function packageJSON($git){
		// $git->query('/')
	}

	public function categorizeCommits($commits)
    {

        foreach ($commits as $index => $commit) {
			if ($this->is_empty_commit($commit)){
				array_push($this->commitCategories['empty'], $commit);
				
			} else if ($this->is_fix_commit($commit)) {
                array_push($this->commitCategories['fix'], $commit);

			} elseif ($this->is_feature_commit($commit)) {
                array_push($this->commitCategories['feature'], $commit);

			} else {
				array_push($this->commitCategories['uncategorized'], $commit);			
			}
        }

        // dd($this->commitCategories);
	}

	/**
	 * Parse CVE data for vulnerabilities or related vulnerabilities
	 *
	 * @param string $query
	 * @return void
	 */
    public function parseCVE($query){
		// if (\Cache::has($query))
		// 	return \Cache::get($query);

		$cve = app_path() . '/CVE/allitems-cvrf-year-2017.xml';
		$xml = simplexml_load_file($cve);
		$xml->registerXPathNamespace('n', 'http://www.icasi.org/CVRF/schema/cvrf/1.1');
		$xml->registerXPathNamespace('v', 'http://www.icasi.org/CVRF/schema/vuln/1.1');

		$lret = $xml->xpath('v:Vulnerability[.//*[contains(text(), "' . $query .'")]]');
		// \Cache::Add($query, (array)$lret, 60*24*365);
		return $lret;
	}

	public function getContributors(){

	}
	
	public function testCases(){

	}

	public function is_empty_commit($commit){
		return (!isset($commit->commit->message) ) || empty($commit->commit->message);
	}

    public function is_fix_commit($commit)
    {
        $pattern = '/fix|resolve|close/';
        return 1==preg_match($pattern, $commit->commit->message);
    }

    public function is_feature_commit($commit)
    {
        $pattern = '/add|feature/';
        return 1==preg_match($pattern, $commit->commit->message);
    }
}
