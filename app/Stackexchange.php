<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Cache;

class StackExchange 
{
	public $repo;
	public $alternatives;
	public $questions;
	public $stats = [
		'count' 	=> 0,
		'views'		=> 0,
		'answered' 	=> 0,
		'withAnswers' => 0,
		'activity' 	=> 0,
		
		
	];
	
	/**
	 * Get all stats from stachexchange
	 *
	 * @return void
	 */
	public function getStats(){
		$this->stats['count'] = count($this->questions);
		
		foreach ($this->questions as $q) {
			if ($q->is_answered) $this->stats['answered']++;
			if ($q->answer_count>0) $this->stats['withAnswers']++;
			$this->stats['views'] += $q->view_count;

			if ($q->is_answered)
				$this->stats['activity'] += ($q->last_activity_date - $q->creation_date);
		}
		
		if ( $this->stats['count'] >0)
 			$this->stats['views'] 		= $this->stats['views'] / $this->stats['count'] ;

		 if ( $this->stats['answered'] >0)	 
			$this->stats['activity'] 	= $this->stats['activity'] / $this->stats['answered'] /60/60/24;
		// $this->stats['activity'] .= ' Days of activity per question on average';
		return $this->stats;
	}

	public function getAlternatives(){
		if (!empty($this->alternatives))
			return $this->alternatives;

		$client = new \GuzzleHttp\Client();

		$res = $client->request('GET', 'http://api.stackexchange.com/2.2/search',[
			'query' => [
				'site' 		=> 'stackoverflow',
				'page' 		=> 1,
				'key' 		=> 'NhE3NrPF7Ziob5K*ZZp5MA((',
				'intitle'	=> "alternative to {$this->repo}",
				'pagesize'	=> 100

			]
		]);
		return $this->alternatives= (\json_decode( (string)$res->getBody()))->items;
	}
	public function getQuestions(){
		if (!empty($this->questions))
			return $this->questions;

		$client = new \GuzzleHttp\Client();

		$res = $client->request('GET', 'http://api.stackexchange.com/2.2/search',[
			'query' => [
				'site' 		=> 'stackoverflow',
				'page' 		=> 1,
				'key' 		=> 'NhE3NrPF7Ziob5K*ZZp5MA((',
				'intitle'	=> $this->repo,
				'pagesize'	=> 100

			]
		]);
		return $this->questions= (\json_decode( (string)$res->getBody()))->items;
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $repo
	 * @return App\StackExchange
	 */
	public static function create( $repo ){
		$cacheID  = md5($repo);
		$StackExchange = Cache::has("stack_{$cacheID}") ? Cache::get("stack_{$cacheID}") : new self();
		$StackExchange->repo=$repo;
		
		if (Cache::has("stack_{$cacheID}"))
			return $StackExchange;

		$StackExchange->getQuestions();
		Cache::add("stack_{$cacheID}", $StackExchange, 60000);		

		return $StackExchange;
	}
	
}
