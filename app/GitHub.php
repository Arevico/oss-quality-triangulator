<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Cache;

//https://arevico@6ff63a9b5168beaa47c7a623745f24a347b8fb73:

/**
 * 
 * 
 * 
 * 
 */
class GitHub
{

    public $author = '';
    public $repo    = '';

    public $repoInfo        = '';
    public $repoOwner       = null;
    public $orgs            = null;
    public $issues          = null;
	public $commits 		= null;

	public $OAUTH 	= '6ff63a9b5168beaa47c7a623745f24a347b8fb73';

	public $readme;
	public $license;

	public $contributors;

	public $packageJSON;

	public function getTrees(){
	    $repo =  $this->repoQuery('git/trees/master?recursive=1&1=');
	    
		return empty($repo) ? (object)array() : $repo->tree;
	}
	/**
	 * Query GitHub
	 *
	 * @param string $path e.g /readme
	 * @param array $query query parameters
	 * @return void
	 */
	public function repoQuery($path, $query = []){
		$cacheID = serialize(func_get_args());
		$cacheID = sha1($cacheID);
		if (\Cache::has($cacheID)) return \Cache::get($cacheID);

		$query['verify'] = false;

		try {
			$client = new \GuzzleHttp\Client();
	
			$res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/{$path}?access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73", $query 	)->getBody();
			
			$ret = json_decode($res);
			\Cache::add($cacheID, $ret, 1440 * 30 * 12);
			return $ret;
		
		} catch(\Exception $x) {
			\Cache::add($cacheID, '', 1440 * 30 * 12);
			
		}
	
		return '';	
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $file
	 * @return void
	 */
	public function getFile($file){
		return $this->repoQuery( $file );
	}

    /* Repo Info
	 ----------------------------------------------------- */
	public function getReadmeFile(){
		try {
		$client = new \GuzzleHttp\Client();
		if ($this->readme) return $this->readme;

        $res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/readme?access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
            ['verify' => false]
		)->getBody();

		return $this->readme = base64_decode((json_decode($res)->content));

		} catch(\Exception $x) {
			
		}

		return '';
	}

	public function getPackageJSON(){
		$client = new \GuzzleHttp\Client();
		if ($this->packageJSON) return $this->packageJSON;

        $res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/package.json?access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
            ['verify' => false]
		)->getBody();
		return $this->packageJSON = base64_decode((json_decode($res)->content));
	}

	public function getLicenseFile(){
		if ($this->license) return $this->license;

		$client = new \GuzzleHttp\Client();
		
        $res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/license?access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
            ['verify' => false]
		)->getBody();
		
		return $this->icense = base64_decode((json_decode($res)->content));	
	}
		
	 public function getCommits(){
		if ($this->commits) {
            return $this->commits;
        }

        $client = new \GuzzleHttp\Client();
		$res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/commits?per_page=100&access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
		[
			'verify' => false
		]
	)->getBody();
		return $this->commits = json_decode($res);
	 }


    /**
     * Get GitHub Issues
     *
     * @return void
     */
    public function getIssues()
    {
        if ($this->issues) {
            return $this->issues;
        }

		$client = new \GuzzleHttp\Client();
		
        $res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/issues?per_page=100&state=all&access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
            ['verify' => false]
        )->getBody();

        return $this->issues = json_decode($res);
    }
    
    /* Author Info
	 ----------------------------------------------------- */
    public function getOrgs()
    {
        if ($this->orgs) return $this->orgs;
        
		$client = new \GuzzleHttp\Client();
		
		$res =(string) $client->request('GET', "https://api.github.com/users/{$this->author}/orgs?access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
			['verify' => false]
		)->getBody();

		return $this->orgs = json_decode($res);
    }

    
    public function __construct($url)
    {
        $url        = preg_replace('(^.*?github\.com[\/]{0,})', "", $url);
        $parts      = explode('/', $url);

        $this->author   = $parts[0];
        $this->repo     = $parts[1];

        $client = new \GuzzleHttp\Client();
		
        $res =(string) $client->request('GET', "https://api.github.com/repos/{$this->author}/{$this->repo}?access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
            ['verify' => false]
        )->getBody();

        $this->repoInfo         = json_decode($res);
	}
	
	public function collaborators(){
		try {
			$client = new \GuzzleHttp\Client();
			if ($this->contributors) return $this->contributors;
	
			$res =(string) $client->request('GET', "api.github.com/repos/{$this->author}/{$this->repo}/contributors?sort=contributions&per_page=100&access_token=6ff63a9b5168beaa47c7a623745f24a347b8fb73",
				['verify' => false]
			)->getBody();
	
			return $this->contributors = (json_decode($res));
	
			} catch(\Exception $x) {
				
			}
	
			return '';	}


}
