<div>
	<h3>Effort Estimation</h3>
	<ul>
		<li v-show="!Risk.testCases" class="warn">The project contains test cases</li>
		<li v-show="Risk.testCases" class="ok">The project contains test cases</li>

		<li v-show="!Risk.ci" class="warn">The project <strong>does not</strong> use continuous integration</li>
		<li v-show="Risk.ci" class="ok">The project uses continuous integration</li>

	</ul>
	<h3>Risk of having insufficient quality </h3>
	<ul>
		<li :class="{warn: MaturityScore.alternatives.length>0}">@{{MaturityScore.alternatives.length }} people searching for alternatives on StackExchange</li>
	</ul>

	<h3>Legal Risks</h3>
	<ul>
		<li v-if="repo.license" class="ok">License detected: @{{ repo.license.name }}</li>
		<li class="warn" v-if="!repo.license">Repository does not include a license file</li>
		<ul>
			<li class="{warn:isLicenseViral, ok:!isLicenseViral}">Viral license detected ? @{{ isLicenseViral ? 'Yes' : 'No' }}</li>
		</ul>
	</ul>
</div>