<div v-show="feedBack.sent==0">
	<div>
		To help up us improve our tool, please answer the following questions:
	</div>

	<h4>How useful is the information provided by this tool ? (1 - 5)</h4>
	<input type="number" placeholder="5" v-model="feedBack['1']"><br />

	<h4>Did the tool influence decision about using this component or not?<br />
	<input type="checkbox" v-model="feedBack['2']" :true-value="'yes'" :false-value="'no'">
		<span v-show="feedBack['2']=='yes'">Yes, it did</span>
		<span v-show="feedBack['2']!='yes'">No, it did not</span>
	</h4>
	<br />	
	
	<div v-show="feedBack['2']">
		In which way?
		<input style="text-align:left;" type="text" name="" id="" v-model="feedBack['3']">
	</div>

	<h4>Did your opinion about the component became more negative, positive or neutral?</h4>
		<select name="" id="" v-model="feedBack['4']">
			<option value="neutral">neutral</option>
			<option value="positive">positive</option>
			<option value="negative">negative</option>
		</select>
	<br />

	<h4>What can we do to improve this tool / are there other things you want to share ?</h4>
	<textarea name="" id="" cols="100" rows="10" v-model="feedBack['6']"></textarea><br />

	<input type="submit" value="- Send Feedback -" @click="sendFeedback">
</div>
<div v-show="feedBack.sent==1">
	<div style="font-style:italic;">Your feedback has been processed, thank you!</div>
</div>
