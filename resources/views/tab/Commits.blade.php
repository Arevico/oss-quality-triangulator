<div v-for="commit in commits">
	[@{{ commit.commit.author.date}}] by <i><span style="color:blue;">@{{ commit.commit.author.name }}</span></i> <br />
	@{{ commit.commit.message }}
	<br />&nbsp;
	</div>